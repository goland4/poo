package main

import "fmt"

// esto es como una interfaz en otros lenguajes como typescript
type Course struct {
	Name    string
	Price   float64
	IsFree  bool
	UserIds []uint
	Classes map[uint]string //le dice que el key es un uint y el value es un string

}

// esta funcion es parte de la estructura Course, cuando use el puntero en una funcion aunque no la actualize nada debo ponerlo igual
func (c *Course) PrintClasses() { //esto es un método que pertenece a la estructura Course
	text := "Las clases son: "
	for _, class := range c.Classes {
		text += class + ", "
	}
	fmt.Println(text[:len(text)-2]) //para quitar la coma y el espacio
}

// le tengo que poner el puntero para que pueda modificar el valor de la estructura
func (c *Course) ChangePrice(price float64) {
	//estoy llamando con el c.Price al precio de la estructura Course y le estoy asignando el precio que le estoy pasando
	c.Price = price
}

//cuando necesitamos actualizar un valor de la estructura, le pasamos el puntero
//cuando trabjamos con los valores sin actualizar, no le pasamos el puntero
